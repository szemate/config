alias ls='ls --color=auto'

# Load completions
autoload -U compinit; compinit

# Load Homebrew
if [ -x /opt/homebrew/bin/brew ]; then
    eval "$(/opt/homebrew/bin/brew shellenv)"
elif [ -x /home/linuxbrew/.linuxbrew/bin/brew ]; then
    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi

# Load Starship prompt
if type starship &>/dev/null; then
    eval "$(starship init zsh)"
fi

# Load Direnv
if type direnv &>/dev/null; then
    eval "$(direnv hook zsh)"
fi
