# dotfiles

Backups of my config files managed with [yadm](https://yadm.io/).

```sh
curl -fsSL https://github.com/yadm-dev/yadm/raw/master/yadm |
    bash -s clone --bootstrap git@codeberg.org:szemate/dotfiles.git
```
