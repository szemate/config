# Source global definitions
if [ -f /etc/bashrc ]; then
    TERM='' . /etc/bashrc
fi

# User specific aliases and functions
alias ls='ls --color=auto'
set editing-mode vi

# Load Homebrew
if [ -x /opt/homebrew/bin/brew ]; then
    eval "$(/opt/homebrew/bin/brew shellenv)"
elif [ -x /home/linuxbrew/.linuxbrew/bin/brew ]; then
    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi

# Load completions
if [ -f /opt/homebrew/etc/profile.d/bash_completion.sh ]; then
    . /opt/homebrew/etc/profile.d/bash_completion.sh
elif [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

# Load Starship prompt
if type starship &>/dev/null; then
    eval "$(starship init bash)"
fi

# Load Direnv
if type direnv &>/dev/null; then
    eval "$(direnv hook bash)"
fi
