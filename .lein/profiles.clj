{:user
 {:aliases {"rebl" ["trampoline" "run" "-m" "rebel-readline.main"]
            "rebl-" ["run" "-m" "rebel-readline.main"]}
  :dependencies [[com.bhauman/rebel-readline "0.1.4"]]
  :plugins [[lein-kibit "0.1.8"]
            [lein-try "0.4.3"]]
  }
 }
