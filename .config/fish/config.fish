functions --copy cd __cd

# cd: Jump to repository root without arguments and list directory contents
function cd
    if test "$argv" != ''
        __cd $argv
    else if set --local rootdir (git rev-parse --show-toplevel 2>/dev/null)
        __cd $rootdir
    else
        __cd ~
    end; and ls
end

# nvim: Raise error if any of the specified files doesn't exist
function nvim
    for arg in $argv
        if string match --quiet --regex '^[^-\+]' -- $arg; and not test -e $arg
            echo (status current-command): $arg: No such file or directory >&2
            return 1
        end
    end
    command nvim $argv
end

function nvim! --description "Create new file(s) with nvim"
    for arg in $argv
        if string match --quiet --regex '^[^-\+]' -- $arg; and test -e $arg
            echo (status current-command): $arg: File already exists >&2
            return 1
        end
    end
    command nvim $argv
end

fish_vi_key_bindings
set fish_greeting ''

# Command aliases
alias diff 'colordiff'
alias df 'df -H'
alias du 'du -h'
alias g 'git'
alias gs 'git status'
alias glow 'glow --pager --width 100'
alias history 'history --show-time="%F %T  " --reverse'
alias jq 'jq -C'
alias less 'less -R'
alias ls 'gls --color=auto --group-directories-first --human-readable --indicator-style=classify'
alias python 'python3'
alias rg 'rg -n'
alias vi 'nvim'
alias vi! 'nvim!'
alias wiki 'nvim +VimwikiIndex'
alias y 'yadm'
alias ys 'yadm status'
alias yet 'yadm enter tig'

# Environment variables
set -x CLICOLOR 1
set -x EDITOR nvim
set -x CARGO_INSTALL_ROOT ~/.local
set -x GOBIN ~/.local/bin
set -x GOPATH ~/.go
set -x GPG_TTY (tty)
set -x NODE_PATH ~/.local/lib/node_modules

# Load Homebrew
if test -f /opt/homebrew/bin/brew
    eval (/opt/homebrew/bin/brew shellenv)
else if test -f /home/linuxbrew/.linuxbrew/bin/brew
    eval (/home/linuxbrew/.linuxbrew/bin/brew shellenv)
end

# Load Nix
if test -f /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish
    source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish
end

# Load Starship prompt
if type -q starship
    starship init fish | source
end

# Load Direnv
if type -q direnv
    direnv hook fish | source
end

# Set fzf.fish key bindings
if type -q fzf_configure_bindings
    fzf_configure_bindings --directory=\cf --git_log=\cg --git_status=\cs \
        --history=\cr --processes=\cp --variables=\cv
end

# Start Tmux
if type -q tmux; and status --is-interactive; and test -z "$TMUX"
    exec tmux new-session
end
