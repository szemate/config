-- AstroCore provides a central place to modify mappings, vim options, autocommands, and more!
-- Configuration documentation can be found with `:h astrocore`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    autocmds = {
      -- first key is the `augroup` (:h augroup)
      gitcommit = {
        {
          -- use custom line length in Git commit
          event = { "FileType" },
          pattern = "gitcommit",
          callback = function()
            vim.bo.textwidth = 72
            vim.opt.colorcolumn = '72'
          end,
        },
      },
      diff = {
        {
          event = { "WinEnter" },
          callback = function()
            if vim.wo.diff then
              -- focus the working copy
              vim.api.nvim_command("wincmd l")
              -- jump to next diff
              vim.api.nvim_set_keymap("n", "<CR>", "]c", {})
              -- jump to previous diff
              vim.api.nvim_set_keymap("n", "<BS>", "[c", {})
            end
          end,
        },
      },
    },
    features = {
      large_buf = { size = 1024 * 256, lines = 10000 }, -- set global limits for large files for disabling features like treesitter
      autopairs = true, -- enable autopairs at start
      cmp = true, -- enable completion at start
      diagnostics_mode = 3, -- diagnostic mode on start (0 = off, 1 = no signs/virtual text, 2 = no virtual text, 3 = on)
      highlighturl = true, -- highlight URLs at start
      notifications = true, -- enable notifications at start
    },
    diagnostics = {
      virtual_text = true,
      underline = true,
    },
    options = {
      o = { -- vim.o.<key>
        nrformats = "alpha",
      },
      opt = { -- vim.opt.<key>
        colorcolumn = "88",
        list = true,
        listchars = {
          extends = "»",
          precedes = "«",
          tab = ">·",
          trail = "•",
        },
        mouse = "",
        number = false,
        relativenumber = false,
        signcolumn = "yes",
        spell = false,
        tabstop = 4,
        timeoutlen = 250,
        wrap = false,
      },
      g = { -- vim.g.<key>
        -- NOTE: `mapleader` and `maplocalleader` must be set in the AstroNvim opts or before `lazy.setup`
        -- This can be found in the `lua/lazy_setup.lua` file
        vimwiki_global_ext = 0,
        vimwiki_list = {
          {
            path = "~/MEGA/VimWiki/",
            syntax = "markdown",
            ext = "md",
          },
        },
      },
    },
    mappings = {
      -- NOTE: keycodes follow the casing in the vimdocs. For example, `<Leader>` must be capitalized
      n = {
        [";"] = { ":" },

        -- navigate buffers
        ["Q"] = { ":bd<CR>", desc = "Close tab" },
        ["T"] = { ":enew<CR>", desc = "New tab" },
        ["<Tab>"] = { ":bnext<CR>", desc = "Next tab" },
        ["<S-Tab>"] = { ":bprev<CR>", desc = "Previous tab" },

        ["<Leader>t"] = {
          function()
            require("symbols-outline").toggle_outline()
          end,
          desc = "Toggle Symbols"
        },

        -- setting a mapping to false will disable it
        ["<C-S>"] = false,
        ["<C-Q>"] = false,
        ["<Leader>o"] = false,
      },
      i = {
        ["<Tab>"] = false,
      },
      v = {
        -- selection till the end of line not to include new line character
        ["$"] = { "$h" },
      },
    },
  },
}
