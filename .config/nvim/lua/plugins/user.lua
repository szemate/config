-- You can also add or configure plugins by creating files in this `plugins/` folder

---@type LazySpec
return {
  -- customize default plugins
  {
    "akinsho/toggleterm.nvim",
    enabled = false
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    opts = {
      filesystem = {
        filtered_items = {
          visible = true,
          hide_dotfiles = false,
          hide_gitignored = false,
        },
      },
      event_handlers = {
        {
          event = "file_opened",
          handler = function(_)
            vim.cmd([[Neotree toggle]])
          end,
        },
      },
    },
  },
  {
    "rcarriga/nvim-notify",
    opts = {
      background_colour = "#000000",
    },
  },

  -- add plugins
  { "editorconfig/editorconfig-vim" },
  {
    "gbprod/cutlass.nvim",
    opts = { cut_key = "m" },
  },
  { "HiPhish/rainbow-delimiters.nvim" },
  { "michaeljsmith/vim-indent-object" },
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    opts = {},  -- this causes the plugin setup function to be called
  },
  {
   "rose-pine/neovim",
    name = "rose-pine",
    opts = {
      styles = {
        bold = true,
        italic = false,
        transparency = true,
      },
    },
  },
  {
    "simrat39/symbols-outline.nvim",
    lazy = true,
    opts = {
      auto_close = true,
      autofold_depth = 1,
    },
  },
  {
    "smoka7/multicursors.nvim",
    event = "VeryLazy",
    dependencies = {
      'nvimtools/hydra.nvim',
    },
    opts = {},
    cmd = {
      'MCstart', 'MCvisual', 'MCclear', 'MCpattern', 'MCvisualPattern', 'MCunderCursor'
    },
    keys = {
      {
        mode = { 'v' },
        '<Leader>m',
        '<cmd>MCstart<cr>',
        desc = 'Multicursor mode',
      },
    },
  },
  { "tpope/vim-repeat" },
  { "tpope/vim-surround" },
  { "triglav/vim-visual-increment" },
  {
    "vimwiki/vimwiki",
    cmd = "VimwikiIndex",
  },
}
